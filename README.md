# Bolt Service

Lasse mir dir meinen Bolt CMS Service zeigen. Unter https://boltcms.internetblogger.de ist die Haupt-Service-Webseite, auf der ich Service-Details
und alles andere festhielte. Es geht um Bolt CMS Installationen in Shared Hosting und den Webseitenaufbau mit dem Bolt OpenSource CMS.

Ich und mein Partner befassen sich mit Bolt eher täglich, denn wir arbeiten damit sehr gerne, weil es ein gutes CMS ist. Es ist sehr flexibel, kann
individuell projekte-technisch gestaltet werden und auch an Designs hapert es nicht wirklich. Wir wollen unseren Service herausbringen, indem wir
den potentiellen Kunden dabei helfen, deren erte Webseite mit Bolt CMS zu realisieren. 

Einfach ist es für diese Kunden ja nicht, denn du benötigst wesentlich mehr Vordergrund-Wissen als beim WP-CMS und alles muss erst noch erlernt werden.
Ich zu meinem Teil werde zu deinem Bolt Supportet und supporte dich bei allen technischen Fragen sowie Anliegen. 

## Bestandteile meiner Bolt CMS Dienstleistung

- Webseitenerstellung in Bolt und dauerhafter Aufbau + Kundenbetreuung
- Menüserstellung
- Erstellen von Taxonomien und Datentypen
- Pfelge wichtiger Dateien wie config.yml
- Google Maps Integration
- Favicon Erstellung + Einbinden
- Google XML Sitemap einbinden
- Kontakt-Formulare Erstellung
- Webdesingauswahl + Installation
- Eindeutschen der Pages, Entries, Showcases
- Eindeutschen von Datentypen, Menüs und Taxonomien
- Eindeutschen des Bolt Themes
- und vieles Weitere auf deine Anfrage im Live Chat unter https://internetblogger.de

## Technischer Support von mir erfolgt durch:

- Live Chat auf Internetblogger.de
- aus dem deutschen Festznetz unter +49 381-33795190
- per mobile Nummer unter +49 152-05854957
- via WhatsApp
- via Telegram Web
- via Mail unter alexanderliebrecht@internetblogger.de

Mehr ist nicht machbar, denn das, was ein herkömmlicher Dienstleister anbieten kann, hast du bei mir ebenfalls. 

## Bolt Service Bestell-Möglichkeiten

- via https://internetblogger.de/shop
- via http://boltservicerostock.de
- via https://yaf-forums.de/wcf/shop
- via https://shop.apps-tools-cms.de
- per Mail unter alexanderliebrecht@internetblogger.de

## Zahlungsmöglichkeiten in den jeweiligen Shops

In der Regel alles Mögliche an Zahlungsmethoden verfügbar.

- via Rechnung
- per Banküberweisung
- per PayPal
- via Kreditkarte



## Kosten einer Bolt-Installation und des Webseitenaufbaus

Diese betragen 500+ Euro inkl. der MwSt. von 19% und für Bolt Updates nehme ich 30 Euro pro Update. Weitere Kosten entstehen, wenn ich dauerhaft an deiner Webseite arbeite.
Das wird dann dir in Rechnung gestellt. Ich denke anfangs, wo du noch ein blutiger Bolt-Beginner bist, muss ich dir einiges beibringen. Es ist ein
Prozess, der nie vollendet sein wird, aber etliches kann man lernen, sodass du eines Tages viele Bolt-Aufgaben im Alleingag erledigen kannst. Das ist
mein Ziel bei den Kunden. 

Projekte wie https://anfrage.hausbau.gr habe ich persönlich umgesetzt und der Inhaber ist zugleich mein Bolt-Partner wegen dem Service in Bolt CMS.
Wir bauen solche Webseiten auf Kundenwünsche hin auf und sorgen nach und nach für deren Funktionalität. 

## Bolt Designs

Diese gibt es und ich zeige dir darunter, was es so alles gibt.

![Bolt Design, was es so nicht mehr gibt](https://internetblogger.de/wp-content/uploads/2018/07/bolt-cms-design-gut-für-angebote-offertypes.jpg)

![Bolt Design einer anderen Art](https://internetblogger.de/wp-content/uploads/2018/07/bolt-design-base-2016-bolt-service-via-internetblogger-de.jpg)

![Dunkles Bolt-Theme](https://internetblogger.de/wp-content/uploads/2018/07/bolt-3-5-4-theme-design-internetblogger-de-bolt-cms-service.jpg)

![Bolt Design base-2018](https://internetblogger.de/wp-content/uploads/2018/08/anfrage-hausbau-gr-internetblogger-de-referenz.jpg)

![Bolt Theme mit header-Images](https://internetblogger.de/wp-content/uploads/2018/07/bolttipps-apps-tools-cms-de-webseitenfrontend.jpg)

![Google Maps und Disqus Comments in Bolt CMS](https://internetblogger.de/wp-content/uploads/2018/07/boltc-cms-blogger-eu-disqus-comments-und-google-maps-einbindung.jpg)

![Contact Formular in bolt CMS](https://internetblogger.de/wp-content/uploads/2018/07/kontaktformular-boltcmsservice-de.jpg)

Und viele anderen gibt das offizielle Bolt-Theme-Verzeichnis her, ein Pool, aus welchem ich mir für meine Projekte etwas immer herauspicken kann.

Du musst kein PHPler sein, denn das sind wir auch nicht, ich arbeite nur mit Fertig-CMS, die an mich, meine Bedürfnisse und die Kundenwünsche
angepasst werden. Wozu noch selbst programmieren, wenn du ein stabiles und sicheres OpenSource CMS vom Markt nehmen und damit alles machen 
kannst. 

Ich mache in letzter Zeit sehr rege Service-Promotion, denn wir wollen uns in Deutschland etablieren, sodass auch Unternehmen auf uns aufmerksam
werden. Nur so kann man gute und stetige Kundschaft aufbauen, aber ich fange damit sehr klein an, mit ein paar Kunden, die mir ihre Aufgaben
zukommen lassen. 

Den Bolt Support hast du jederzeit von mir per Mail unter alexanderliebrecht@internetblogger.de .

## Impressum und Kontakt

    Alexander Liebrecht Internet Services
    Alexander Liebrecht
    Alte Warnenmünder Ch. 25
    D-18109 Rostock
    Mail: alexanderliebrecht@internetblogger.de
    WWW: https://internetblogger.de und https://yaf-forums.de
    Bolt Service: https://boltcms.internetblogger.de

> Ich würde mich sehr auf die Zusammenarbeit mit dir freuen, denn man lebt nur einmal. Warum nicht dann alles in die Bolt Webseite investieren.

